; -*- Mode: Emacs-Lisp ; Coding: utf-8 -*-

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

;; 参考：https://qiita.com/yoshikyoto/items/3f5de63648febe897bee
;; load-path

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

;; idoを起動
(ido-mode 1)
(ido-everywhere 1)

;; 中間/あいまい一致
(setq ido-enable-flex-matching t) 

;; C-hをDELETEに
(define-key key-translation-map [?\C-h] [?\C-?])

(when load-file-name
  (setq user-emacs-directory (file-name-directory load-file-name)))

; 半角英字設定
(set-face-attribute 'default nil :family "Source Code Pro for Powerline" :height 130)
; 全角かな設定
(set-fontset-font (frame-parameter nil 'font)
                  'japanese-jisx0208
                  (font-spec :family "Noto Sans CJK JP" :size 14))
; 半角ｶﾅ設定
(set-fontset-font (frame-parameter nil 'font)
                  'katakana-jisx0201
                  (font-spec :family "Noto Sans CJK JP" :size 14))

;;記号をデフォルトのフォントにしない
(setq use-default-font-for-symbols nil)

;; Dracula Themeを使用
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'dracula t)

(global-linum-mode t)
(setq linum-format "%4d |")

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
;; marmalade certificate expired.
;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

;; 拡張子で適用するモードを変更する
(add-to-list 'auto-mode-alist '("\\.rb\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.adoc\\'" . adoc-mode))

(when (require 'skk nil t)
  (set-language-environment "Japanese")
  (prefer-coding-system 'utf-8)
  (global-set-key "\C-\\" 'skk-mode)
  (global-set-key (kbd "C-x j") 'skk-auto-fill-mode) ;;良い感じに改行を自動入力してくれる機能
  (setq default-input-method "japanese-skk"))         ;;emacs上での日本語入力にskkをつかう 

;; 海外で人気の自動補完、comany-modeの設定
(global-company-mode +1)

;; 自動補完を offにしたい場合は, company-idle-delayを nilに設定する
;; auto-completeでいうところの ac-auto-start にあたる.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-idle-delay nil)
 '(package-selected-packages
   (quote
    (ac-skk ddskk ddskk-posframe ido-skk ac-math auctex-latexmk latex-extra latex-math-preview latex-unicode-math-mode math-symbol-lists org-edit-latex px company adoc-mode pandoc web-mode vue-mode powerline neotree magit indent-guide flycheck dirtree auto-complete add-node-modules-path))))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
